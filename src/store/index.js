import Vue from 'vue'
import Vuex from 'vuex'
import state from './state'
import mutations from './mutations'

Vue.use(Vuex)

export default new Vuex.Store({
	state,
	mutations
})
/*流程就是组件调用actions，actions调用mutations, mutations去改变数据*/